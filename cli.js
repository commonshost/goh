#!/usr/bin/env node

const { readFileSync } = require('fs')
const yargs = require('yargs')
const { server } = require('./server')

const { argv } = yargs
  .env('GOH')
  .option('log-level', {
    type: 'string',
    describe: 'Log output volume',
    default: 'info',
    choices: ['silent', 'info']
  })
  .option('gopher-allow-non-standard-port', {
    type: 'boolean',
    describe: 'Connect to Gopher hosts on ports other than 70',
    default: false
  })
  .option('gopher-allow-private-address', {
    type: 'boolean',
    describe: 'Connect to Gopher hosts on a private IP address',
    default: false
  })
  .option('gopher-connection-timeout', {
    type: 'number',
    describe: 'Maximum idle time on the Gopher connection',
    default: 10000
  })
  .option('gopher-handshake-timeout', {
    type: 'number',
    describe: 'Maximum duration of the TLS connection establishment',
    default: 10000
  })
  .option('http', {
    type: 'boolean',
    describe: 'Accept plaintext HTTP connections',
    default: true
  })
  .option('http-port', {
    type: 'number',
    describe: 'Port for HTTP connections',
    default: 7080
  })
  .option('https', {
    type: 'boolean',
    describe: 'Accept encrypted HTTPS connections',
    default: false
  })
  .option('https-port', {
    type: 'number',
    describe: 'Port for HTTPS connections',
    default: 7443
  })
  .option('https-public-certificate', {
    type: 'string',
    describe: 'File path of the TLS public certficate',
    default: ''
  })
  .option('https-private-key', {
    type: 'string',
    describe: 'File path of the TLS private key',
    default: ''
  })
  .option('https-certificate-authority', {
    type: 'array',
    describe: 'File paths of the CA certificate chain',
    default: []
  })
  .option('tls-origin-cache-max-ttl', {
    type: 'number',
    describe: 'Origin TLS probe cache expiry',
    default: 24 * 60 * 60 * 1000
  })
  .config()
  .version()
  .help()
  .wrap(null)

const options = {
  logLevel: argv.logLevel,
  goh: {
    allowHTTP1: true,
    unsafeAllowNonStandardPort: argv.gopherAllowNonStandardPort,
    unsafeAllowPrivateAddress: argv.gopherAllowPrivateAddress,
    timeout: argv.gopherConnectionTimeout,
    handshakeTimeout: argv.gopherHandshakeTimeout,
    originCacheMaxAge: argv.tlsOriginCacheMaxTtl
  }
}

if (argv.http) {
  options.http = {
    port: argv.httpPort
  }
}

if (argv.https) {
  if (!argv.httpsPublicCertificate) {
    console.error('Missing option: --https-public-certificate')
    process.exit(1)
  }
  if (!argv.httpsPrivateKey) {
    console.error('Missing option: --https-private-key')
    process.exit(1)
  }
  if (!argv.httpsCertificateAuthority) {
    console.error('Missing option: --https-certificate-authority')
    process.exit(1)
  }
  options.https = {
    port: argv.httpsPort,
    cert: readFileSync(argv.httpsPublicCertificate),
    key: readFileSync(argv.httpsPrivateKey),
    ca: argv.httpsCertificateAuthority.map((ca) => readFileSync(ca))
  }
}

server(options)
