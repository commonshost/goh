const { goh } = require('.')
const connect = require('connect')
const http1 = require('http')
const http2 = require('http2')
const { once } = require('events')

function log (request, response, next) {
  console.log(request.method, request.url)
  next()
}

function cors (request, response, next) {
  response.setHeader(
    'Access-Control-Allow-Origin',
    '*'
  )
  next()
}

function catchall (request, response, next) {
  response.write('This is a Gopher over HTTP (GoH) service.\n')
  response.write('For details, see: https://gitlab.com/commonshost/goh\n')
  if (request.headers.accept !== 'application/gopher') {
    response.write('Missing request header: Accept: application/gopher\n')
  }
  const url = new URL(request.url, `http://${request.headers.host}`)
  if (!url.searchParams.has('url')) {
    response.write('Missing query parameter: url\n')
  }
  response.end()
}

async function server (options) {
  const middleware = goh(options.goh)

  const app = connect()
  if (options.logLevel === 'info') {
    app.use(log)
  }
  app.use(cors)
  app.use(middleware)
  app.use(catchall)

  for (const [scheme, createServer] of Object.entries({
    http: http1.createServer,
    https: http2.createSecureServer
  })) {
    if (options[scheme]) {
      const server = createServer(options[scheme], app)
      server.listen(options[scheme].port)
      await once(server, 'listening')
      const { port } = server.address()
      console.log(`Listening on ${scheme}://localhost:${port}`)
    }
  }
}

module.exports.server = server
