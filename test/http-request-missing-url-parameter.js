const test = require('blue-tape')
const { goh } = require('..')
const { promisify } = require('util')

test('Missing request URL', async (t) => {
  const request = {
    headers: { accept: 'application/gopher' },
    httpVersionMajor: 2,
    method: 'GET',
    url: `/`
  }

  const response = {
    writeHead: () => t.fail()
  }

  const middleware = goh({
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true
  })

  await t.shouldFail(
    promisify(middleware)(request, response), // next(error)
    /Bad Request/
  )
})
