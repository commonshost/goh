const test = require('blue-tape')
const { goh } = require('..')
const { once } = require('events')
const connect = require('connect')
const { context } = require('fetch-h2')
const http2 = require('http2')
const {
  createGopherTcpServer,
  middlewareEchoError
} = require('./helpers')
const readline = require('readline')

test('Fall through to plaintext if TLS times out', async (t) => {
  // Gopher server is silent until valid selector is received
  const gopherServer = createGopherTcpServer(async (client) => {
    const reader = readline.createInterface({ input: client })
    const line = await once(reader, 'line')
    const validSelector = /^[\w\s]*$/
    if (!line || validSelector.test(line)) {
      console.log('Line is a valid selector')
      reader.close()
      client.end('Hello, World!')
      await once(client, 'close')
      console.log('CLIENT CLOSED')
    } else {
      console.log('Line is not a valid selector')
    }
  })
  gopherServer.listen(0)
  await once(gopherServer, 'listening')
  const { port: gopherPort } = gopherServer.address()
  console.log(`Gopher line based server at gopher://localhost:${gopherPort}`)

  const tlsSocketOptions = {
    rejectUnauthorized: false
  }
  const middleware = goh({
    allowHTTP1: true,
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true,
    handshakeTimeout: 1000,
    ...tlsSocketOptions
  })
  const app = connect()
  app.use(middleware)
  app.use(middlewareEchoError)

  const httpServer = http2.createServer(app)
  httpServer.listen(0)
  await once(httpServer, 'listening')
  const { port: httpPort } = httpServer.address()
  console.log(`HTTP server at http://localhost:${httpPort}`)

  const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}`)
  const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
  const client = context({ httpProtocol: 'http2' })
  const response = await client.fetch(url, {
    headers: { accept: 'application/gopher' }
  })

  t.is(response.ok, true)
  const body = await response.arrayBuffer()
  t.ok(body.byteLength)
  const decoded = String.fromCharCode.apply(null, new Uint8Array(body))
  t.is(decoded, 'Hello, World!')

  await client.disconnectAll()
  httpServer.close()
  await once(httpServer, 'close')

  gopherServer.close()
  await once(gopherServer, 'close')
})

// test('Problematic URL', async (t) => {
//   const middleware = goh({
//     handshakeTimeout: 2000
//   })
//   const app = connect()
//   app.use(middleware)
//   app.use(middlewareEchoError)

//   const httpServer = http2.createServer(app)
//   httpServer.listen(0)
//   await once(httpServer, 'listening')
//   const { port: httpPort } = httpServer.address()
//   console.log(`HTTP server at http://localhost:${httpPort}`)

//   const gopherUrl = encodeURIComponent('gopher://r-36.net/I/s/gopher.png')
//   const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
//   const client = context({ httpProtocol: 'http2' })
//   const response = await client.fetch(url, {
//     headers: { accept: 'application/gopher' }
//   })

//   t.is(response.ok, true)
//   const body = await response.arrayBuffer()
//   t.ok(body.byteLength > 0)

//   await client.disconnectAll()
//   httpServer.close()
//   await once(httpServer, 'close')
// })
