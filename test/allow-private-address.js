const test = require('blue-tape')
const { goh } = require('..')
const { promisify } = require('util')

test('Deny connection to private IP address', async (t) => {
  const url = 'gopher://localhost'
  const request = {
    headers: { accept: 'application/gopher' },
    httpVersionMajor: 2,
    method: 'GET',
    url: `/?url=${encodeURIComponent(url)}`
  }

  const response = {
    writeHead: () => t.fail()
  }

  const middleware = goh({
    unsafeAllowPrivateAddress: false
  })

  await t.shouldFail(
    promisify(middleware)(request, response), // next(error)
    /Forbidden/
  )
})
