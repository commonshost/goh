const test = require('blue-tape')
const { goh } = require('..')
const { promisify } = require('util')

test('Hostname not found', async (t) => {
  const url = 'gopher://localhost.does-not-exist'
  const request = {
    headers: { accept: 'application/gopher' },
    httpVersionMajor: 2,
    method: 'GET',
    url: `/?url=${encodeURIComponent(url)}`
  }

  const response = {
    writeHead: () => t.fail()
  }

  const middleware = goh()

  await t.shouldFail(
    promisify(middleware)(request, response), // next(error)
    /Bad Gateway/
  )
})
