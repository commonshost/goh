const test = require('blue-tape')
const { goh } = require('..')
const { once } = require('events')
const connect = require('connect')
const { context } = require('fetch-h2')
const http2 = require('http2')
const {
  createGopherTcpServer,
  sendGopherResponse,
  middlewareEchoError
} = require('./helpers')

test('Trim item type from selector in URL', async (t) => {
  const gopherServer = createGopherTcpServer(sendGopherResponse)
  gopherServer.listen(0)
  await once(gopherServer, 'listening')
  const { port: gopherPort } = gopherServer.address()
  console.log(`Gopher/tcp at gopher://localhost:${gopherPort}`)

  const middleware = goh({
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true
  })
  const app = connect()
  app.use(middleware)
  app.use(middlewareEchoError)

  const httpServer = http2.createServer(app)
  httpServer.listen(0)
  await once(httpServer, 'listening')
  const { port: httpPort } = httpServer.address()
  console.log(`HTTP server at http://localhost:${httpPort}`)

  const client = context({ httpProtocol: 'http2' })

  {
    let attempts = 0
    const counter = () => attempts++
    gopherServer.on('connection', counter)
    const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}/0/README`)
    const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
    const response = await client.fetch(url, {
      headers: { accept: 'application/gopher' }
    })
    const body = await response.text()
    console.log(body)
    t.ok(body.includes('Selector: /README\t'))
    t.is(attempts, 2)
    gopherServer.off('connection', counter)
  }

  {
    let attempts = 0
    const counter = () => attempts++
    gopherServer.on('connection', counter)
    const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}/0/README`)
    const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
    await client.fetch(url, {
      headers: { accept: 'application/gopher' }
    })
    t.is(attempts, 1)
    gopherServer.off('connection', counter)
  }

  {
    // Leading slash is not added unnecessarily
    const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}/1no-leading-slash`)
    const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
    const response = await client.fetch(url, {
      headers: { accept: 'application/gopher' }
    })
    const body = await response.text()
    console.log(body)
    t.ok(body.includes('Selector: no-leading-slash\t'))
  }

  {
    // Allow a blank type and selector
    const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}`)
    const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
    const response = await client.fetch(url, {
      headers: { accept: 'application/gopher' }
    })
    const body = await response.text()
    console.log(body)
    t.ok(body.includes('Selector: \t'))
  }

  {
    const invalidGopherUrl = 'example://not.a.gopher.url'
    const url = `http://localhost:${httpPort}/?url=${invalidGopherUrl}`
    const response = await client.fetch(url, {
      headers: { accept: 'application/gopher' }
    })
    t.is(response.ok, false)
    t.is(response.status, http2.constants.HTTP_STATUS_BAD_REQUEST)
    const body = await response.text()
    t.is(body, 'Bad Request')
  }

  {
    const invalidMethod = 'POST'
    const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}`)
    const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
    const response = await client.fetch(url, {
      headers: { accept: 'application/gopher' },
      method: invalidMethod
    })
    t.is(response.ok, false)
    t.is(response.status, http2.constants.HTTP_STATUS_METHOD_NOT_ALLOWED)
    const body = await response.text()
    t.is(body, 'Method Not Allowed')
  }

  await client.disconnectAll()
  httpServer.close()
  await once(httpServer, 'close')

  gopherServer.close()
  await once(gopherServer, 'close')
})
