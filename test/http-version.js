const test = require('blue-tape')
const { goh } = require('..')
const { promisify } = require('util')

test('Reject HTTP/1 clients', async (t) => {
  const url = 'gopher://localhost'
  const request = {
    headers: { accept: 'application/gopher' },
    httpVersionMajor: 1,
    method: 'GET',
    url: `/?url=${encodeURIComponent(url)}`
  }

  const response = {
    writeHead: () => t.fail()
  }

  const middleware = goh({
    allowHTTP1: false
  })

  await t.shouldFail(
    promisify(middleware)(request, response), // next(error)
    /Version Not Supported/
  )
})
