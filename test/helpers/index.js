const fs = require('fs')
const net = require('net')
const tls = require('tls')

function middlewareEchoError (error, request, response, next) {
  response.statusCode = error.statusCode || 500
  response.end(error.message)
}

async function sendGopherResponse (client) {
  client.on('data', (chunk) => {
    client.write('iHello, World!\t\thost\t1\r\n')
    const selector = chunk.toString().replace(/[\r\n]+$/, '')
    client.write(`iSelector: ${selector}\t\thost\t1\r\n`)
    client.end()
  })
}

function createGopherTcpServer (serverOptions, connectionListener) {
  if (typeof serverOptions === 'function') {
    connectionListener = serverOptions
    serverOptions = {}
  }
  return net.createServer(serverOptions, connectionListener)
}

const tlsServerOptions = {
  ecdhCurve: 'P-384:P-256',
  key: fs.readFileSync('key.pem'),
  cert: fs.readFileSync('cert.pem'),
  ca: [/* fs.readFileSync('ca.pem') */]
}

function createGopherTlsServer (serverOptions, connectionListener) {
  if (typeof serverOptions === 'function') {
    connectionListener = serverOptions
    serverOptions = {}
  }
  const server = tls.createServer({
    ALPNProtocols: ['gopher'],
    ...tlsServerOptions,
    ...serverOptions
  }, connectionListener)
  return server
}

module.exports = {
  tlsServerOptions,
  createGopherTcpServer,
  createGopherTlsServer,
  sendGopherResponse,
  middlewareEchoError
}
