const test = require('blue-tape')
const { goh } = require('..')
const { promisify } = require('util')

test('Ignore if request does not accept `application/gopher`', async (t) => {
  const url = 'gopher://localhost'
  const request = {
    headers: { accept: 'example/this-is-not-gopher' },
    httpVersionMajor: 2,
    method: 'GET',
    url: `/?url=${encodeURIComponent(url)}`
  }

  const response = {
    writeHead: () => t.fail()
  }

  const middleware = goh({
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true
  })

  const error = await promisify(middleware)(request, response) // next(error)
  t.is(error, undefined)
})
