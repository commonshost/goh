const test = require('blue-tape')
const { goh } = require('..')
const { once } = require('events')
const tls = require('tls')
const connect = require('connect')
const { context } = require('fetch-h2')
const http1 = require('http')
const {
  tlsServerOptions,
  middlewareEchoError
} = require('./helpers')

for (const tlsVersion of ['TLSv1.2', 'TLSv1.3']) {
  test(`Resume ${tlsVersion} session`, async (t) => {
    const echoTls = (client) => {
      client.on('data', (chunk) => {
        client.write(`iTLS: ${client.getProtocol()}\t\thost\t1\r\n`)
        client.write(`iResume: ${client.isSessionReused()}\t\thost\t1\r\n`)
        client.end()
      })
    }
    const gopherServer = tls.createServer({
      minVersion: tlsVersion,
      maxVersion: tlsVersion,
      ALPNProtocols: ['gopher'],
      ...tlsServerOptions
    }, echoTls)
    gopherServer.listen(0)
    await once(gopherServer, 'listening')
    const { port: gopherPort } = gopherServer.address()
    console.log(
      `Unresponsive ${tlsVersion} server at gopher://localhost:${gopherPort}`
    )

    const tlsSocketOptions = {
      rejectUnauthorized: false
    }
    const middleware = goh({
      allowHTTP1: true,
      unsafeAllowNonStandardPort: true,
      unsafeAllowPrivateAddress: true,
      ...tlsSocketOptions
    })
    const app = connect()
    app.use(middleware)
    app.use(middlewareEchoError)

    const httpServer = http1.createServer(app)
    httpServer.listen(0)
    await once(httpServer, 'listening')
    const { port: httpPort } = httpServer.address()
    console.log(`HTTP server at http://localhost:${httpPort}`)

    const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}`)
    const url = `http://localhost:${httpPort}/?url=${gopherUrl}`

    for (const isSessionResumed of [false, true]) {
      {
        const client = context({ httpProtocol: 'http1' })
        const response = await client.fetch(url, {
          headers: { accept: 'application/gopher' }
        })
        t.is(response.ok, true)
        const body = await response.text()
        t.ok(body.includes(`TLS: ${tlsVersion}`))
        t.ok(body.includes(`Resume: ${isSessionResumed}`))
        await client.disconnectAll()
      }
    }

    httpServer.close()
    await once(httpServer, 'close')

    gopherServer.close()
    await once(gopherServer, 'close')
  })
}
