const test = require('blue-tape')
const { match } = require('assert')
const { goh } = require('..')
const { once } = require('events')
const connect = require('connect')
const { context } = require('fetch-h2')
const http2 = require('http2')
const {
  createGopherTcpServer,
  createGopherTlsServer,
  sendGopherResponse
} = require('./helpers')

const scenarios = [
  {
    label: 'Indicate Gopher protocol',
    createGopherServer: createGopherTcpServer,
    expected: /^Gopher\/RFC1436 /
  },
  {
    label: 'Indicate TLS',
    createGopherServer: createGopherTlsServer,
    expected: / TLSv\d\.\d$/
  },
  {
    label: 'Indicate TLS version 1.3',
    createGopherServer: createGopherTlsServer,
    serverOptions: {
      minVersion: 'TLSv1.3',
      maxVersion: 'TLSv1.3'
    },
    expected: / TLSv1\.3$/
  },
  {
    label: 'Indicate TLS version 1.2',
    createGopherServer: createGopherTlsServer,
    serverOptions: {
      minVersion: 'TLSv1.2',
      maxVersion: 'TLSv1.2'
    },
    expected: / TLSv1\.2$/
  },
  {
    label: 'Plaintext Gopher does not indicate TLS',
    createGopherServer: createGopherTcpServer,
    expected: /^Gopher\/RFC1436 localhost:\d+$/
  },
  {
    label: 'Omit standard Gopher port 70',
    createGopherServer: createGopherTcpServer,
    port: 70,
    expected: /^Gopher\/RFC1436 localhost$/
  },
  {
    label: 'Indicate non-standard Gopher port number',
    createGopherServer: createGopherTcpServer,
    port: 0,
    expected: / localhost:\d{4,5}$/
  },
  {
    label: 'Append to existing Via value',
    createGopherServer: createGopherTcpServer,
    httpMiddleware: (request, response, next) => {
      response.setHeader('via', '2.0 hello-world')
      next()
    },
    expected: /^2\.0 hello-world, Gopher\/RFC1436 localhost/
  }
]

for (const {
  label,
  createGopherServer,
  serverOptions = {},
  port = 0,
  httpMiddleware = (request, response, next) => next(),
  expected
} of scenarios) {
  test(`Response Via header: ${label}`, async (t) => {
    const gopherServer = createGopherServer(serverOptions, sendGopherResponse)
    gopherServer.listen(port)
    await once(gopherServer, 'listening')
    const { port: gopherPort } = gopherServer.address()
    console.log(`Gopher at gopher://localhost:${gopherPort}`)

    const tlsSocketOptions = {
      rejectUnauthorized: false
    }
    const middleware = goh({
      allowHTTP1: true,
      unsafeAllowNonStandardPort: true,
      unsafeAllowPrivateAddress: true,
      ...tlsSocketOptions
    })
    const app = connect()
    app.use(httpMiddleware)
    app.use(middleware)

    const httpServer = http2.createServer(app)
    httpServer.listen(0)
    await once(httpServer, 'listening')
    const { port: httpPort } = httpServer.address()
    console.log(`HTTP server at http://localhost:${httpPort}`)

    const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}`)
    const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
    const client = context({ httpProtocol: 'http2' })
    const response = await client.fetch(url, {
      headers: { accept: 'application/gopher' }
    })

    const actual = response.headers.get('via')
    console.log('Via:', actual)
    match(actual, expected)

    const exposeHeaders = response.headers.get('access-control-expose-headers')
    t.ok(exposeHeaders.split(', ').includes('via'))

    await client.disconnectAll()
    httpServer.close()
    await once(httpServer, 'close')

    gopherServer.close()
    await once(gopherServer, 'close')
  })
}
