const test = require('blue-tape')
const { promisify } = require('util')
const { once } = require('events')
const connect = require('connect')
const { goh } = require('..')
const http1 = require('http')
const {
  middlewareEchoError,
  createGopherTcpServer,
  sendGopherResponse
} = require('./helpers')

const sleep = promisify(setTimeout)

test('Cache TLS support', async (t) => {
  const gopherServer = createGopherTcpServer(sendGopherResponse)
  gopherServer.listen(0)
  await once(gopherServer, 'listening')
  const { port: gopherPort } = gopherServer.address()
  console.log(`Gopher/TCP server at gopher://localhost:${gopherPort}`)

  const tlsSocketOptions = {
    rejectUnauthorized: false
  }
  const middleware = goh({
    allowHTTP1: true,
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true,
    originCacheMaxAge: 100, // ms
    ...tlsSocketOptions
  })
  const app = connect()
  app.use(middleware)
  app.use(middlewareEchoError)

  const httpServer = http1.createServer(app)
  httpServer.listen(0)
  await once(httpServer, 'listening')
  const { port: httpPort } = httpServer.address()
  console.log(`HTTP server at http://localhost:${httpPort}`)

  async function connectClient () {
    const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}`)
    const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
    const request = http1.request(url, {
      headers: { accept: 'application/gopher' }
    })
    request.end()
    const [response] = await once(request, 'response')
    t.is(response.statusCode, 200)
    response.resume()
    await once(response, 'close')
  }

  // Count connection attempts
  let attempts
  const counter = () => attempts++
  gopherServer.on('connection', counter)

  // Unknown origin: Probe TLS fails, reconnect as plaintext TCP
  attempts = 0
  await connectClient()
  t.is(attempts, 2)

  // Cached TLS probe: Don't scan TLS again, only connect plaintext TCP
  attempts = 0
  await connectClient()
  t.is(attempts, 1)

  // Wait until cache TTL expires
  await sleep(100)

  // Probe for TLS again, then reconnect as plaintext TCP
  attempts = 0
  await connectClient()
  t.is(attempts, 2)

  httpServer.close()
  await once(httpServer, 'close')

  gopherServer.close()
  await once(gopherServer, 'close')
})
