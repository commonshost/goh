const test = require('blue-tape')
const { goh } = require('..')
const { once } = require('events')
const tls = require('tls')
const connect = require('connect')
const { context } = require('fetch-h2')
const http1 = require('http')
const {
  tlsServerOptions,
  middlewareEchoError
} = require('./helpers')

test('Reject invalid ALPN', async (t) => {
  const ignoreConnections = (client) => {
    client.resume()
  }
  const gopherServer = tls.createServer({
    ALPNProtocols: ['definitely-not-gopher'],
    ...tlsServerOptions
  }, ignoreConnections)
  gopherServer.listen(0)
  await once(gopherServer, 'listening')
  const { port: gopherPort } = gopherServer.address()
  console.log(`Unresponsive TLS server at gopher://localhost:${gopherPort}`)

  const tlsSocketOptions = {
    rejectUnauthorized: false
  }
  const middleware = goh({
    allowHTTP1: true,
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true,
    ...tlsSocketOptions
  })
  const app = connect()
  app.use(middleware)
  app.use(middlewareEchoError)

  const httpServer = http1.createServer(app)
  httpServer.listen(0)
  await once(httpServer, 'listening')
  const { port: httpPort } = httpServer.address()
  console.log(`HTTP server at http://localhost:${httpPort}`)

  const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}/0/README`)
  const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
  const client = context({ httpProtocol: 'http1' })
  const response = await client.fetch(url, {
    headers: { accept: 'application/gopher' }
  })

  t.is(response.ok, false)
  const body = await response.text()
  t.is(body, 'Bad Gateway')

  await client.disconnectAll()
  httpServer.close()
  await once(httpServer, 'close')

  gopherServer.close()
  await once(gopherServer, 'close')
})
