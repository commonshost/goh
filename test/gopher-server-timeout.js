const test = require('blue-tape')
const { goh } = require('..')
const { once } = require('events')
const connect = require('connect')
const { context } = require('fetch-h2')
const http1 = require('http')
const {
  createGopherTlsServer,
  middlewareEchoError
} = require('./helpers')

test('Gopher server timeout', async (t) => {
  const ignoreConnections = (client) => {
    client.resume()
  }
  const gopherServer = createGopherTlsServer(ignoreConnections)
  gopherServer.listen(0)
  await once(gopherServer, 'listening')
  const { port: gopherPort } = gopherServer.address()
  console.log(`Unresponsive TLS server at gopher://localhost:${gopherPort}`)

  const before = Date.now()
  const connectionTimeout = 500
  const tlsSocketOptions = {
    rejectUnauthorized: false
  }
  const middleware = goh({
    allowHTTP1: true,
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true,
    timeout: connectionTimeout,
    ...tlsSocketOptions
  })
  const app = connect()
  app.use(middleware)
  app.use(middlewareEchoError)

  const httpServer = http1.createServer(app)
  httpServer.listen(0)
  await once(httpServer, 'listening')
  const { port: httpPort } = httpServer.address()
  console.log(`HTTP server at http://localhost:${httpPort}`)

  const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}/0/README`)
  const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
  const client = context({ httpProtocol: 'http1' })
  const response = await client.fetch(url, {
    headers: { accept: 'application/gopher' }
  })

  const after = Date.now()
  t.ok(after >= before + connectionTimeout)
  t.is(response.ok, false)
  const body = await response.text()
  t.is(body, 'Gateway Timeout')

  await client.disconnectAll()
  httpServer.close()
  await once(httpServer, 'close')

  gopherServer.close()
  await once(gopherServer, 'close')
})
