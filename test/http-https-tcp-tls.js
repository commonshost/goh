const test = require('blue-tape')
const { goh } = require('..')
const { once } = require('events')
const connect = require('connect')
const { context } = require('fetch-h2')
const http1 = require('http')
const http2 = require('http2')
const {
  createGopherTcpServer,
  createGopherTlsServer,
  sendGopherResponse
} = require('./helpers')

for (const [protocol, http] of Object.entries({ http1, http2 })) {
  for (const [transport, createGopherServer] of Object.entries({
    tcp: createGopherTcpServer,
    tls: createGopherTlsServer
  })) {
    test(`GoH request using ${protocol} and Gopher/${transport}`, async (t) => {
      const gopherServer = createGopherServer(sendGopherResponse)
      gopherServer.listen(0)
      await once(gopherServer, 'listening')
      const { port: gopherPort } = gopherServer.address()
      console.log(`Gopher/${transport} at gopher://localhost:${gopherPort}`)

      const tlsSocketOptions = {
        rejectUnauthorized: false
      }
      const middleware = goh({
        allowHTTP1: true,
        unsafeAllowNonStandardPort: true,
        unsafeAllowPrivateAddress: true,
        ...tlsSocketOptions
      })
      const app = connect()
      app.use(middleware)

      const httpServer = http.createServer(app)
      httpServer.listen(0)
      await once(httpServer, 'listening')
      const { port: httpPort } = httpServer.address()
      console.log(`HTTP server at http://localhost:${httpPort}`)

      const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}`)
      const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
      const client = context({ httpProtocol: protocol })
      const response = await client.fetch(url, {
        headers: { accept: 'application/gopher' }
      })

      t.ok(response.ok)
      t.is(response.status, 200)
      t.is(response.headers.get('content-type'), 'application/gopher')

      const body = await response.text()
      t.ok(body.includes('Hello, World!'))

      await client.disconnectAll()
      httpServer.close()
      await once(httpServer, 'close')

      gopherServer.close()
      await once(gopherServer, 'close')
    })
  }
}
