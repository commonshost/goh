const test = require('blue-tape')
const { goh } = require('..')
const { once } = require('events')
const tls = require('tls')
const connect = require('connect')
const { context } = require('fetch-h2')
const http1 = require('http')
const {
  createGopherTcpServer,
  tlsServerOptions,
  middlewareEchoError
} = require('./helpers')

test('Gopher server breaks client connection', async (t) => {
  const gopherServer = createGopherTcpServer((netClient) => {
    const tlsClient = new tls.TLSSocket(netClient, {
      isServer: true,
      server: gopherServer,
      ALPNProtocols: ['gopher'],
      rejectUnauthorized: false,
      ...tlsServerOptions
    })
    tlsClient.on('secure', () => {
      process.nextTick(() => {
        netClient.write('Raw data to break TLS session')
        netClient.destroy()
      })
    })
  })
  gopherServer.listen(0)
  await once(gopherServer, 'listening')
  const { port: gopherPort } = gopherServer.address()
  console.log(`Bad TLS server at gopher://localhost:${gopherPort}`)

  const tlsSocketOptions = {
    rejectUnauthorized: false
  }
  const middleware = goh({
    allowHTTP1: true,
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true,
    ...tlsSocketOptions
  })
  const app = connect()
  app.use(middleware)
  app.use(middlewareEchoError)

  const httpServer = http1.createServer(app)
  httpServer.listen(0)
  await once(httpServer, 'listening')
  const { port: httpPort } = httpServer.address()
  console.log(`HTTP server at http://localhost:${httpPort}`)

  const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}/0/README`)
  const url = `http://localhost:${httpPort}/?url=${gopherUrl}`
  const client = context({ httpProtocol: 'http1' })
  const response = await client.fetch(url, {
    headers: { accept: 'application/gopher' }
  })

  t.is(response.ok, false)
  const body = await response.text()
  t.is(body, 'Bad Gateway')

  await client.disconnectAll()
  httpServer.close()
  await once(httpServer, 'close')

  gopherServer.close()
  await once(gopherServer, 'close')
})
