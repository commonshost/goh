const test = require('blue-tape')
const { goh } = require('..')
const { once } = require('events')
const connect = require('connect')
const http1 = require('http')
const {
  createGopherTcpServer,
  middlewareEchoError
} = require('./helpers')

test('HTTP client disconnects mid-flight', async (t) => {
  const trickleData = (client) => {
    client.resume()
    client.once('error', console.log)
    const trickle = setInterval(() => {
      const payload = String(Math.random())
      console.log('GOPHER SEND', payload)
      client.write(payload)
    }, 100)
    client.on('close', () => clearInterval(trickle))
  }
  const gopherServer = createGopherTcpServer(trickleData)
  gopherServer.listen(0)
  await once(gopherServer, 'listening')
  const { port: gopherPort } = gopherServer.address()
  console.log(`Bad TLS server at gopher://localhost:${gopherPort}`)

  const tlsSocketOptions = {
    rejectUnauthorized: false
  }
  const middleware = goh({
    allowHTTP1: true,
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true,
    ...tlsSocketOptions
  })
  const app = connect()
  app.use(middleware)
  app.use(middlewareEchoError)

  const httpServer = http1.createServer(app)
  httpServer.listen(0)
  await once(httpServer, 'listening')
  const { port: httpPort } = httpServer.address()
  console.log(`HTTP server at http://localhost:${httpPort}`)

  const gopherUrl = encodeURIComponent(`gopher://localhost:${gopherPort}/0/README`)
  const url = `http://localhost:${httpPort}/?url=${gopherUrl}`

  const request = http1.request(url, {
    headers: { accept: 'application/gopher' }
  })
  request.end()
  const [response] = await once(request, 'response')
  t.is(response.statusCode, 200)
  let chunkCounter = 0
  response.on('data', (chunk) => {
    if (++chunkCounter > 5) {
      request.abort()
    }
  })
  await once(request, 'abort')
  t.ok(chunkCounter >= 5)

  httpServer.close()
  await once(httpServer, 'close')

  gopherServer.close()
  await once(gopherServer, 'close')
})
