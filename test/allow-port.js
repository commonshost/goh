const test = require('blue-tape')
const { goh } = require('..')
const { promisify } = require('util')

test('Deny connection to port other than 70 (Gopher default)', async (t) => {
  const nonStandardPort = 12345
  const url = `gopher://localhost:${nonStandardPort}`
  const request = {
    headers: { accept: 'application/gopher' },
    httpVersionMajor: 2,
    method: 'GET',
    url: `/?url=${encodeURIComponent(url)}`
  }

  const response = {
    writeHead: () => t.fail()
  }

  const middleware = goh({
    unsafeAllowNonStandardPort: false
  })

  await t.shouldFail(
    promisify(middleware)(request, response), // next(error)
    /Forbidden/
  )
})
