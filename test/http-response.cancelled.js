const test = require('blue-tape')
const { goh } = require('..')
const { once } = require('events')
const {
  createGopherTcpServer,
  sendGopherResponse
} = require('./helpers')

test('Gracefully handle cancelled HTTP response', async (t) => {
  const gopherServer = createGopherTcpServer(sendGopherResponse)
  gopherServer.listen(0)
  await once(gopherServer, 'listening')
  const { port: gopherPort } = gopherServer.address()
  console.log(`Gopher/tcp at gopher://localhost:${gopherPort}`)

  const url = `gopher://localhost:${gopherPort}`
  const request = {
    headers: { accept: 'application/gopher' },
    httpVersionMajor: 2,
    method: 'GET',
    url: `/?url=${encodeURIComponent(url)}`
  }

  const response = {
    socket: { destroyed: true },
    writeHead: () => t.fail()
  }

  const next = () => t.fail()

  const middleware = goh({
    unsafeAllowNonStandardPort: true,
    unsafeAllowPrivateAddress: true
  })

  await middleware(request, response, next)

  gopherServer.close()
  await once(gopherServer, 'close')
})
