const net = require('net')
const tls = require('tls')
const { once } = require('events')
const isPrivate = require('private-ip')
const LRU = require('lru-cache')

const {
  BadRequest,
  Forbidden,
  MethodNotAllowed,
  BadGateway,
  GatewayTimeout,
  HTTPVersionNotSupported
} = require('http-errors')

const {
  constants: {
    HTTP2_HEADER_CONTENT_TYPE,
    HTTP2_HEADER_ACCEPT,
    HTTP2_HEADER_VIA,
    HTTP2_METHOD_GET,
    HTTP_STATUS_OK
  }
} = require('http2')

const GOPHER_PROTOCOL = 'Gopher/RFC1436'
const GOPHER_PLAIN = false
const GOPHER_TLS = true
const GOPHER_DEFAULT_PORT = 70
const CRLF = '\r\n'
const GOH_MEDIA_TYPE = 'application/gopher'
const GOH_MINIMUM_HTTP_VERSION_MAJOR = 2

const gopherUrlRegex =
  /^gopher:\/\/(?<host>[^:/]+)(?::(?<port>\d{1,5}))?(?<path>(?:|.*))$/
function parseGopherUrl (url) {
  if (!url || typeof url !== 'string') return null
  const match = gopherUrlRegex.exec(url)
  return match ? match.groups : null
}

async function checkAllowedAddress (socket, options) {
  let address
  try {
    // Errors emit the `lookup` AND `error` events.
    // Catch the `error` event if the lookup has an error.
    address = await new Promise((resolve, reject) => {
      socket.once('lookup', (error, address) => {
        if (error) {
          socket.once('error', (error) => {
            reject(error)
          })
        } else {
          resolve(address)
        }
      })
    })
  } catch (error) {
    socket.destroy()
    throw new BadGateway()
  }

  if (options.unsafeAllowPrivateAddress === false && isPrivate(address)) {
    socket.destroy()
    throw new Forbidden()
  }
}

function connectTimeout (socket, event, options) {
  return new Promise(async (resolve, reject) => {
    const timer = setTimeout(() => {
      socket.destroy(new GatewayTimeout())
    }, options.handshakeTimeout)
    try {
      await once(socket, event)
    } catch (error) {
      clearTimeout(timer)
      reject(error)
      return
    }
    clearTimeout(timer)
    resolve()
  })
}

async function netConnect (port, host, options) {
  const netSocket = new net.Socket(options)
  netSocket.connect(port, host)
  await checkAllowedAddress(netSocket, options)
  await connectTimeout(netSocket, 'connect', options)
  return netSocket
}

async function tlsConnect (port, host, options, session) {
  const tlsSocket = tls.connect({
    port,
    host,
    servername: host,
    session,
    ALPNProtocols: ['gopher'],
    ...options
  })
  await checkAllowedAddress(tlsSocket, options)
  await connectTimeout(tlsSocket, 'secureConnect', options)
  if (tlsSocket.alpnProtocol !== 'gopher') {
    tlsSocket.destroy()
    throw new BadGateway()
  }
  return tlsSocket
}

async function connectGopher (origins, tlsSessions, port, host, options) {
  const origin = host + ':' + port
  const protocol = origins.get(origin)

  let socket
  switch (protocol) {
    case GOPHER_PLAIN:
      socket = await netConnect(port, host, options)
      break
    default:
    case GOPHER_TLS:
      const session = tlsSessions.get(origin)
      try {
        socket = await tlsConnect(port, host, options, session)
        origins.set(origin, GOPHER_TLS)
        if (socket.getProtocol() === 'TLSv1.2') {
          const tlsSession = socket.getSession()
          tlsSessions.set(origin, tlsSession)
        }
        socket.on('session', (tlsSession) => {
          tlsSessions.set(origin, tlsSession)
        })
      } catch (error) {
        if ('statusCode' in error && !(error instanceof GatewayTimeout)) {
          throw error
        }
        origins.set(origin, GOPHER_PLAIN)
        socket = await netConnect(port, host, options)
      }
      break
  }

  socket.once('error', () => origins.del(origin))
  return socket
}

module.exports.goh =
function goh ({
  allowHTTP1 = false,
  unsafeAllowNonStandardPort = false,
  unsafeAllowPrivateAddress = false,
  handshakeTimeout = 10000,
  timeout = 10000,
  originCacheMaxAge = 24 * 60 * 60 * 1000,
  ...options
} = {}) {
  const origins = new LRU({
    max: 1000,
    maxAge: originCacheMaxAge
  })

  const tlsSessions = new LRU({
    max: 1000
  })

  return async function goh (request, response, next) {
    if (request.headers[HTTP2_HEADER_ACCEPT] !== GOH_MEDIA_TYPE) {
      return next()
    }
    if (
      request.httpVersionMajor < GOH_MINIMUM_HTTP_VERSION_MAJOR &&
      allowHTTP1 === false
    ) {
      return next(new HTTPVersionNotSupported())
    }
    if (request.method !== HTTP2_METHOD_GET) {
      return next(new MethodNotAllowed())
    }

    const url = parseGopherUrl(new URLSearchParams(
      request.url.substr(request.url.indexOf('?'))
    ).get('url'))

    if (url === null) {
      return next(new BadRequest())
    }

    const { host, path } = url
    const port = url.port ? parseInt(url.port) : GOPHER_DEFAULT_PORT
    if (
      unsafeAllowNonStandardPort === false &&
      port !== GOPHER_DEFAULT_PORT
    ) {
      return next(new Forbidden())
    }
    const selector = (path.startsWith('/') && path.length >= 2)
      ? path.substr(2) : path

    let socket
    try {
      socket = await connectGopher(origins, tlsSessions, port, host, {
        unsafeAllowPrivateAddress,
        handshakeTimeout,
        ...options
      })
    } catch (error) {
      next(error)
      return
    }

    if (
      request.aborted ||
      (response.stream && response.stream.destroyed) ||
      (response.socket && response.socket.destroyed)
    ) {
      socket.end()
      return
    }

    response.once('close', () => {
      if ((response.stream || response).writableEnded === false) {
        socket.destroy()
        next(new Error())
      }
    })

    socket.once('error', () => {
      socket.destroy()
      next(new BadGateway())
    })

    socket.setTimeout(timeout, () => {
      socket.end()
      next(new GatewayTimeout())
    })

    socket.write(selector + CRLF)
    socket.once('data', (chunk) => {
      const received = GOPHER_PROTOCOL + ' ' +
        (port === GOPHER_DEFAULT_PORT ? host : `${host}:${port}`) +
        (socket instanceof tls.TLSSocket ? ' ' + socket.getProtocol() : '')
      const via = response.hasHeader(HTTP2_HEADER_VIA)
        ? [response.getHeader(HTTP2_HEADER_VIA), received].flat() : received
      response.writeHead(HTTP_STATUS_OK, {
        'access-control-expose-headers': 'via',
        [HTTP2_HEADER_CONTENT_TYPE]: GOH_MEDIA_TYPE,
        [HTTP2_HEADER_VIA]: via
      })
      response.write(chunk)
      socket.pipe(response.stream || response)
    })
  }
}
